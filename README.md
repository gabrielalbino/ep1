# Aplicação do Conway's Game of Life em C++

## Sobre o autor:

Gabriel Batista Albino Silva, 16/0028361

## O que é o Conway's Game of Life?

Você pode encontrar informações sobre o projeto nesse link: [Conway's_Game_of_Life on Wikipedia](http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

## Como utilizar

Clone o respositório utilizando **git clone**, compile utilizando o **make** e execulte-o com o **make run**

## Itens do menu

No menu você pode fazer quantas alterações desejar, ao finalizar as alterações no campo celular digite 0 para inserir o numero de gerações que suas celulas deverão iterar.

Uma breve descrição dos itens do menu:

**0 - Encerrar Modificações no universo celular**

Acaba com a possíbilidade de modificar o campo celular, perguntando quantas iterações você deseja e iniciando o jogo.

**1 - Adicionar uma celula no universo celular**

Adiciona uma única celula na posição escolhida do seu campo celular.

Exemplo:

![1](https://i.imgur.com/sBb6g3j.png)

**2 - Adicionar um block no universo celular**

Adiciona um Block na posição escolhida do seu campo celular.

Exemplo:

![1](https://i.imgur.com/Wkx7pC2.png)

**3 - Adicionar um blinker no universo celular**

Adiciona um Blinker na posição escolhida do seu campo celular.

Exemplo:

![1](https://i.imgur.com/vR3Uk4k.png)

**4 - Adicionar um glider no universo celular**

Adiciona um Glider na posição escolhida do seu campo celular.

Exemplo:

![1](https://i.imgur.com/Loz5ght.png)

**5 - Adicionar uma gosperGliderGun no universo celular**

Adiciona uma GosperGliderGun na posição escolhida do seu campo celular.

Exemplo:

![1](https://i.imgur.com/SHyXsy1.png)

**6 - Remover uma celula do universo celular**

Remove uma celula na posição escolhida do seu campo celular.

**7 - Remover um block do universo celular**

Remove um Block na posição escolhida do seu campo celular.

**8 - Remover um blinker do universo celular**

Remove um Blinker na posição escolhida do seu campo celular.

**9 - Remover um glider do universo celular**

Remove um Glider na posição escolhida do seu campo celular.

**10 - Remover uma gosperGliderGun no universo celular**

Remove uma GosperGliderGun na posição escolhida do seu campo celular.